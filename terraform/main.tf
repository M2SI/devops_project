terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile                 = "default"
  region                  = "us-east-2"
  shared_credentials_file = "./.credential-ynov"
}

variable "instance_name" {
  description = "Instance name"
  type        = string
  default     = "Mostacci_app"
}

variable "sshkey_dir" {
  description = "Set the directory where the public ssh key is."
  type        = string
  default     = "/mnt/c/Users/Greg/.ssh/id_rsa.pub"
}

data "template_file" "user_data" {
  template = file("./scripts/add-ssh-web-app.yaml")
}

resource "aws_instance" "app_server" {
  count = 1
  ami           = "ami-0d97ef13c06b05a19"
  instance_type = "t2.micro"
  associate_public_ip_address = true
  user_data                   = data.template_file.user_data.rendered
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name   = "ssh_Mostacci"

  tags = {
    Name   = "${var.instance_name}"
    Groups = "app"
    Owner  = "MostacciGregory"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "ssh_Mostacci"
  public_key = file(var.sshkey_dir)
}

resource "aws_security_group" "allow_ssh" {
  name        = "mostacci_allow_ssh"
  description = "Allow TLS inbound traffic"

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "WEB from VPC"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}




output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.app_server.*.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.app_server.*.public_ip
}
#ssh -i ssh/id_rsa_deploy centos@ec2-3-133-124-178.us-east-2.compute.amazonaws.com

# ssh -i /tmp/id_rsa centos@18.224.169.81  