#!groovy

pipeline {
    agent any
    tools {
        maven 'maven'
    }
    environment {
        TEST = 'TEST'
    }
    options {
        buildDiscarder(logRotator(numToKeepStr: '100'))
        ansiColor('xterm')
    }
    stages {
        stage('Job description') {
            steps {
                script {
                    println('Env var: ' + env.TEST)
                    sh 'java --version'
                    sh 'mvn --version'
                    sh 'python3 --version'
                    currentBuild.displayName = "#${BUILD_NUMBER}"
                }
            }
        }
        stage('start terraform') {
            steps {
                sh 'cd /usr/share/terraform/ && terraform init'
                sh 'cd /usr/share/terraform/ && terraform apply --auto-approve'
            }
        }
    }
}
