#!groovy

pipeline {
    agent any
    tools {
        maven 'maven'
    }
    environment {
        TEST = 'TEST'
    }
    options {
        buildDiscarder(logRotator(numToKeepStr: '100'))
        ansiColor('xterm')
    }
    stages {
        stage('Job description') {
            steps {
                script {
                    println('Env var: ' + env.TEST)
                    sh 'java --version'
                    sh 'mvn --version'
                    sh 'python3 --version'
                    currentBuild.displayName = "#${BUILD_NUMBER}"
                }
            }
        }
        stage('Git clone') {
            steps {
                git branch: "${params.BRANCH}",
                        url: 'https://github.com/Ozz007/sb3t.git'
            }
        }
        stage('Compile') {
            steps {
                sh 'mvn compile'
            }
        }
        stage('Unit test') {
            when {
                expression { params.SKIP_TESTS == false }
            }
            steps {
                sh 'mvn test'
            }
        }
        stage('Package') {
            steps {
                sh 'mvn package'
            }
        }
        stage('Integration test') {
            when {
                expression { params.SKIP_TESTS == false }
            }
            
            steps {
                sh 'mvn verify'
            }
        }
        stage('movefile') {
            steps {
                sh 'mv /var/jenkins_home/jobs/CI/jobs/Job1/workspace/sb3t-ws/target/sb3t-ws-1.0-SNAPSHOT.jar /var/jenkins_home/jobs/CI/jobs/Job1/workspace/'
            }
        }
    }
}
