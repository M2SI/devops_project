#!groovy
println('------------------------------------------------------------------Import Job CI/Job1')
def pipelineScript = new File('/var/jenkins_config/jobs/job1-pipeline.groovy').getText("UTF-8")

pipelineJob('CI/Job1') {
    description("Job Pipline 1")
    parameters {
        stringParam {
            name('BRANCH')
            defaultValue('master')
            description("Sélection de la branche du repo Github")
            trim(false)
        }
       booleanParam{
           name('SKIP_TESTS')
           defaultValue(false)
           description("Contrôle d'exécution des tests")
       }
       stringParam {
           name('VERSION_TYPE')
           defaultValue('SNAPSHOT')
           description("SNAPSHOT ou RELEASE")
           trim(false)
       }
       stringParam {
           name('VERSION')
           defaultValue('SB3T-1.0-SNAPSHOT')
           description("Version du jar")
           trim(false)
       }
    }
    definition {
        cps {
            script(pipelineScript)
            sandbox()
        }
    }
}
