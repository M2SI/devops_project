# Devops Projet from Ynov Lyon


## Installation

You have to install Docker before continue and pull the repository.

## Usage

Run docker: 
```bash
docker-compose up --build
```

Let's start your browser on localhost:80.
From Jenkins, you can start the differents jenkins pipeline to start the project

* job1.pipeline => create the jar file
* terraform.pipeline => create the aws instance
* ~~ansible.pipeline => not existing~~